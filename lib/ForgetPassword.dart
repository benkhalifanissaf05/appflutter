import 'package:flutter/material.dart';
import 'package:myappflutter/ForgetPassword.dart';
import 'package:myappflutter/Home.dart';
import 'package:myappflutter/Register.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("ForgetPassword"),
          centerTitle: true,

          ///automaticallyImplyLeading: false,

          actions: <Widget>[
            TextButton.icon(
                onPressed: () {
                  print("okkkk not");
                },
                icon: Icon(
                  Icons.notification_add,
                  color: Colors.white,
                ),
                label: Text("Nt")),
          ],
        ),
        body: Center(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(20.0),
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Forget Password',
                      style: TextStyle(fontSize: 20),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Confirmer'),
                      onPressed: () {
                        print(emailController.text);
                        print(passwordController.text);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                   Home(title: "home Page")),
                        );
                      },
                    )),
              ],
            ),
          ),
        ));
  }
}

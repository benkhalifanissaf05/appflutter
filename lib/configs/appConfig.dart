
const base_url = 'http://localhost:3000/';
const base_url_category = base_url + "/categories/";
const base_url_auth = base_url + "/auth/";
const base_url_subcategory = base_url + "/subcategories/";
const base_url_gallery= base_url + "/galleries/";
const base_url_product = base_url + "/products/";
const base_url_order= base_url + "/orders/";
const base_url_customer = base_url + "/customers/";
const base_url_provider = base_url + "/providers/";
const base_url_user= base_url + "/users/";
const base_url_dellivery= base_url + "/deliveries/";

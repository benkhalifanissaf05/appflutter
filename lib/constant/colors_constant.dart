import 'package:flutter/cupertino.dart';

class AppColors {
      AppColors._();
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color lightPink = Color(0xFFFAC0BE);
  static const Color cherryRed = Color(0xFFFA2424);
 

  // Dark Theme colors
  static const Color darkGrey = Color(0xff303041);
  static const Color lightGrey = Color(0xFF3D3A50);
  static const Color blue = Color(0xFF0EA2F6);
  static const Color burgundy = Color(0xFF880d1e);
  static const Color spaceCadet = Color(0xFFF4FCFE);

  // Light Theme Colors
  static const Color babyPink = Color.fromARGB(255, 6, 93, 92);
  static const Color lavender = Color.fromARGB(255, 32, 119, 113);
  static const Color gunMetal = Color(0xFF545677);
  static const Color spaceBlue = Color(0xFF03254E);
  static const Color darkBlue = Color(0xFF011C27);
}
import 'package:get/get.dart';
import 'package:myappflutter/controllers/AuthController.dart';
import 'package:myappflutter/controllers/CategoryController.dart';
import 'package:myappflutter/controllers/CustomerController.dart';
import 'package:myappflutter/controllers/DeliveryController.dart';
import 'package:myappflutter/controllers/GalleryController.dart';
import 'package:myappflutter/controllers/OrderController.dart';
import 'package:myappflutter/controllers/ProductController.dart';
import 'package:myappflutter/controllers/ProviderController.dart';
import 'package:myappflutter/controllers/SubCategoryController.dart';
import 'package:myappflutter/controllers/UserController.dart';

class BaseController {
  final authController = Get.put(AuthController());
  final categoryController = Get.put(CategoryController());
  final customerController = Get.put(CustomerController);
  final deliveryController = Get.put(DeliveryController());
  final galleryController = Get.put(GalleryController());
 final orderController = Get.put(OrderController());
 final productController = Get.put(ProductController());
 final providerController = Get.put(ProviderController());
  final subcategoryController = Get.put(SubCategoryController());
final userController = Get.put(UserController());





}

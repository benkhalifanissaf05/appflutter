import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myappflutter/configs/appConfig.dart';

class CategoryController extends GetxController{
//creaCategory
 final _dio = Dio();
  createCategory(data) async {
    return await _dio.post(base_url_category, data: data);
  }
  updateCategory(data) async {
    return await _dio.patch(base_url_category, data: data);
  }
  removeCategory(id) async {
    return await _dio.delete(base_url_category + id);
  }
  getByIdCategory(id) async {
   return await _dio.get(base_url_category+'getbyid' + id);
  }
  getByNameCategory(name) async {
    return await _dio.get(base_url_category+'getname' + name);
  }

}

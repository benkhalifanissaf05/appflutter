import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myappflutter/configs/appConfig.dart';

class GalleryController extends GetxController{
  final dio = Dio();
  creatGallery(data) async {
    return await dio.post(base_url_gallery  , data: data);
  }

  getGallery() async {
    return await dio.get(base_url_gallery);
  }


}
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myappflutter/configs/appConfig.dart';

class CustomerController extends GetxController {
  final dio = Dio();
  creatCustomer(data) async {
    return await dio.post(base_url_customer  , data: data);
  }

  getCustomer() async {
    return await dio.get(base_url_customer);
  }






}

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myappflutter/configs/appConfig.dart';

class ProductController  extends GetxController {
  final dio = Dio ();
  creatProduct(data) async {
    return await dio.post(base_url_product , data: data);
  }

  getProduct() async {
    return await dio.get(base_url_product);
  }

  
}
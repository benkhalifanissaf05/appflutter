import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myappflutter/configs/appConfig.dart';

class AuthController extends GetxController {
  final _dio = Dio();
  signUp(data) async {
    return await _dio.post(base_url_auth, data: data);
  }
}

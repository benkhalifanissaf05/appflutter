import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myappflutter/constant/theme.dart';
import 'package:myappflutter/controllers/theme_controller.dart';
import 'dart:developer';
import 'constant/dimens.dart' as example;
import 'constant/strings.dart' as example;
import 'constant/styles.dart';
import 'package:floating_bottom_bar/animated_bottom_navigation_bar.dart';
import 'constant/colors.dart' as example;


class Home extends StatefulWidget {
   Home({Key? key, required this.title})
      : super(key: key);
final String title;
 
  @override
  State<Home> createState() => _HomePage();
}
 
   final themeController = Get.find<ThemeController >();

class _HomePage extends State<Home> {
  bool circleButtonToggle = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading:false,
              title: Text(widget.title),
            actions: [
          IconButton(
            onPressed: () {
              if (Get.isDarkMode) {
               themeController.changeTheme(Themes.lightTheme);
                 themeController.saveTheme(false);
              } else {
                themeController.changeTheme(Themes.darkTheme);
                themeController.saveTheme(true);
              }
            },
            icon: Get.isDarkMode
                ? const Icon(Icons.light_mode_outlined)
                : const Icon(Icons.dark_mode_outlined),
          ),
        ],
            ),
            floatingActionButton: const SizedBox(
              height: example.Dimens.heightNormal,
              width: example.Dimens.widthNormal,
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            body: const Center(child: Text('Text')),
            bottomNavigationBar: AnimatedBottomNavigationBar(
              bottomBarItems: [
                BottomBarItemsModel(
                  icon: const Icon(Icons.home, size: example.Dimens.iconNormal),
                  iconSelected: const Icon(Icons.home,
                      size: example.Dimens.iconNormal),
                  title: example.Strings.home,
                  dotColor: example.AppColors.cherryRed,
                  onTap: () {
                    log('Home');
                  },
                ),
                BottomBarItemsModel(
                  icon:
                      const Icon(Icons.search, size: example.Dimens.iconNormal),
                  iconSelected: const Icon(Icons.search,
                      color: AppColors.cherryRed,
                      size: example.Dimens.iconNormal),
                  title: example.Strings.search,
                  dotColor: example.AppColors.cherryRed,
                  onTap: () {
                    log('Search');
                  },
                ),
                BottomBarItemsModel(
                  icon:
                      const Icon(Icons.person, size: example.Dimens.iconNormal),
                  iconSelected: const Icon(Icons.person,
                      color: AppColors.cherryRed,
                      size: example.Dimens.iconNormal),
                  title: example.Strings.person,
                  dotColor: example.AppColors.cherryRed,
                  onTap: () {
                    log('Profile');
                  },
                ),
                BottomBarItemsModel(
                    icon: const Icon(Icons.settings,
                        size: example.Dimens.iconNormal),
                    iconSelected: const Icon(Icons.settings,
                        color: AppColors.cherryRed,
                        size: example.Dimens.iconNormal),
                    title: example.Strings.settings,
                    dotColor: example.AppColors.cherryRed,
                    onTap: () {
                      log('Settings');
                    }),
              ],
              bottomBarCenterModel: BottomBarCenterModel(
                centerBackgroundColor: example.AppColors.cherryRed,
                centerIcon: const FloatingCenterButton(
                  child: Icon(
                    Icons.add,
                    color: AppColors.white,
                  ),
                ),
                centerIconChild: [
                  FloatingCenterButtonChild(
                    child: const Icon(
                      Icons.home,
                      color: AppColors.white,
                    ),
                    onTap: () => log('Item1'),
                  ),
                  FloatingCenterButtonChild(
                    child: const Icon(
                      Icons.home,
                      color: AppColors.white,
                    ),
                    onTap: () => log('Item2'),
                  ),
                  FloatingCenterButtonChild(
                    child: const Icon(
                      Icons.home,
                      color: AppColors.white,
                    ),
                    onTap: () => log('Item3'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
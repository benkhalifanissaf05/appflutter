
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:myappflutter/Home.dart';
import 'package:myappflutter/Login.dart';
import 'package:myappflutter/Playground.dart';
import 'package:myappflutter/Register.dart';
import 'package:myappflutter/SplashScreen.dart';
import 'package:myappflutter/constant/theme.dart';
import 'package:myappflutter/controllers/AuthController.dart';
import 'package:myappflutter/controllers/theme_controller.dart';
import 'package:myappflutter/intro_screen_default.dart';
//import 'package:myappflutter/Playground.dart';

import 'constant/dimens.dart' as example;
import 'constant/strings.dart' as example;
import 'constant/styles.dart';
import 'package:floating_bottom_bar/animated_bottom_navigation_bar.dart';

import 'constant/colors.dart' as example;
import 'package:flutter/services.dart';
import 'package:myappflutter/changetheme.dart';

void main() async {
  await GetStorage.init(); // important
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
 //trés importants
    final themeController = Get.put(ThemeController());
  final authController = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
 
    // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: example.Strings.appName,
      //theme: ThemeData(fontFamily: Fonts.poppins), themeMode: themeController.theme,
      themeMode: themeController.theme,
     theme: Themes.lightTheme,
    darkTheme: Themes.darkTheme,
       // home:Home(title: example.Strings.appName),
     initialRoute: '/',
      routes: {
        '/': (context) => SplashScreen(),
        /*  '/slide': (context) => IntroScreenDefault(),*/
        '/home': (context) => Home(title: example.Strings.appName),
        '/login': (context) => Login(),
        '/register': (context) => Register(),
       // '/theme': (context) => MyWidget(),
      },
      //home: const BottomNavigatorExample(title: example.Strings.appName),
    );
  }
}
